package com.hiver.athena.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Topic {
    private String name;
    private List<Question> questions;

    public Topic(String name) {this.name=name;}
}
