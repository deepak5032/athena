package com.hiver.athena.entity;

public enum Difficulty {
    EASY, MEDIUM, HARD;

    public static Difficulty valueOfLabel(String label) {
        for (Difficulty difficulty : values()) {
            if (difficulty.toString().equalsIgnoreCase(label)) {
                return difficulty;
            }
        }
        return null;
    }

}
