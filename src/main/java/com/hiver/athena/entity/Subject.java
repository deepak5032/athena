package com.hiver.athena.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Subject {
    private String name;
    private List<Topic> topics;
    private List<Question> questions;

    public Subject(String name) {
        this.name=name;
    }
}
