package com.hiver.athena.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Question {
    private String text;
    private Subject subject;
    private Topic topic;
    private Difficulty difficulty;
    private int marks;
}
