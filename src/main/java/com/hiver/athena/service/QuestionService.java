package com.hiver.athena.service;

import com.hiver.athena.entity.Difficulty;
import com.hiver.athena.entity.Question;
import com.hiver.athena.entity.Subject;
import com.hiver.athena.entity.Topic;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

@Service
public class QuestionService {
    private static List<Question> questionStore= new ArrayList<>();
    private final String fileName="/questions.csv";

    public void addQuestion(Question question) {
        questionStore.add(question);
    }

    public boolean removeQuestion(Question question) {
        return questionStore.remove(question);
    }

    public List<Question> getAllQuestion() {
        if(questionStore.isEmpty())
            addQuestionsFromCsv();
        return questionStore;
    }

    public void reshuffleQuestions() {
        Collections.shuffle(questionStore);
    }

    private void addQuestionsFromCsv() {
        String line = "";
        String splitBy = ",";
        try {
            InputStream inputStream = getClass().getResourceAsStream(fileName);
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            while ((line = br.readLine()) != null) {
                String[] questionParams = line.split(splitBy);
                Question question = new Question(questionParams[0].trim(), new Subject(questionParams[1].trim()),
                        new Topic(questionParams[2].trim()),
                        Difficulty.valueOfLabel(questionParams[3].trim()), Integer.parseInt(questionParams[4].trim()));
                questionStore.add(question);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
