package com.hiver.athena.service;

import com.hiver.athena.constant.Constants;
import com.hiver.athena.entity.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class QuestionPaperGenerator {

    @Autowired
    private QuestionService questionService;

    public List<Question> generateQuestionPaper(int marks, String parameter, String filtersParam) {
        List<Question> questionPaper= new ArrayList<>();
        List<Question> allQuestions=questionService.getAllQuestion();
        List<String> filterParams = Arrays.asList(filtersParam.split(",").clone());
        Map<String,Integer> marksMap=new HashMap<>();
        for(String filterParam:filterParams) {
            String[] paramArray = filterParam.split(" ");
            marksMap.put(paramArray[1].toLowerCase(),(marks * Integer.parseInt(paramArray[0])) / 100);
        }

        if (parameter.equalsIgnoreCase(Constants.DIFFICULTY)) {
            for (Question question : allQuestions) {
                int currentMarks = question.getMarks();
                if(marksMap.get(question.getDifficulty().name().toLowerCase())>=currentMarks) {
                    questionPaper.add(question);
                    marksMap.put(question.getDifficulty().name().toLowerCase(),
                            marksMap.get(question.getDifficulty().name().toLowerCase())-currentMarks);
                }
            }
        }
        questionService.reshuffleQuestions();
        return questionPaper;
    }
}
