package com.hiver.athena.constant;

public class Constants {
    public static final String DIFFICULTY="difficulty";
    public static final String TOPIC="topic";
    public static final String SUBJECT="subject";
}
