package com.hiver.athena.rest;

import com.hiver.athena.entity.Question;
import com.hiver.athena.service.QuestionPaperGenerator;
import com.hiver.athena.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/questions")
public class QuestionPaperRest {

    @Autowired
    private QuestionPaperGenerator questionPaperGenerator;

    @Autowired
    private QuestionService questionService;

    @GetMapping("/generatePaper/{marks}/{param}/{filterParams}")
    public List<Question> getQuestionpaper(@PathVariable String marks, @PathVariable String param,
                                           @PathVariable String filterParams) {
        return questionPaperGenerator.generateQuestionPaper(Integer.parseInt(marks.split(" ")[0]),
                param, filterParams);
    }

    @GetMapping("/check")
    public String checkApi() {
        return "api is working";
    }

    @PostMapping("/addQuestion")
    public void addQuestion(@RequestBody Question question) {
        questionService.addQuestion(question);
    }

    @DeleteMapping("/removeQuestion")
    public boolean removeQuestion(@RequestBody Question question) {
        return questionService.removeQuestion(question);
    }
}
